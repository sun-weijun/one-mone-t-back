<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use DateTime;

class Index extends Frontend
{

    protected $noNeedLogin = [];
    protected $noNeedRight = '*';
    protected $layout = '';

    public function index()
    {
        $auth = $this->auth;

        $schooid = $auth->__get('school_id');

  
        $schooname = Db::name('school')->where('id', $schooid)->find();

        // $schooname =  Db::name('school')->where('id', $schooid)->find();
        $year = date('Y');
        $month = ltrim(date('m'), '0');
        // var_dump($year);
        // var_dump($month);
        // 构建查询条件s
        $record = Db::name('task')
            ->where('year', $year)
            ->where('month', 01)
            ->find();
        
         //判断是否已经上交
       
       $this->view->assign('age',  $this->calculateAge($auth->__get('birthday')));
        if ($record) {
          $is_put=  DB::name('student_reports')->where('user_id', $auth->id)->where('task_id',$record['id'])->where('status',1)->find();
          if ($is_put) {
              $this->view->assign('is_put', true);
          } else {
              $this->view->assign('is_put', false);
          }
            // 如果记录存在
         $this->view->assign('record', $record);
        } else {
            // 如果记录不存在
            $this->view->assign('record', false);
        }


        if ($schooname) {
            $this->view->assign('schoolname', $schooname);
        }
        if ($this->request->isPost()) {

            $naked_left = filter_var($this->request->post('naked_left'), FILTER_VALIDATE_FLOAT);
            $naked_right = filter_var($this->request->post('naked_right'), FILTER_VALIDATE_FLOAT);
            $glasses_left = filter_var($this->request->post('glasses_left'), FILTER_VALIDATE_FLOAT);
            $glasses_right = filter_var($this->request->post('glasses_right'), FILTER_VALIDATE_FLOAT);
            // $schoolname = $this->request->post('schoolname');
       
            $task_id=$this->request->post('task_id');
            $mobile=$this->request->post('mobile');
            $grade=$this->request->post('grade');
            $classd=$this->request->post('classd');
            $card=$this->request->post('idcard');
            $ethnicity = $this->request->post('ethnicity');
        
            // 检查是否所有变量都是有效的整数
            if (($naked_left !== false && $naked_right !== false) || ($glasses_left !== false && $glasses_right !== false)) {
    
} else {
    return $this->error('至少提交一项数据且补全，请检查');
}         
            $reportData = [
                'idcard'=>$card,
                'naked_left' => $naked_left ,
                'naked_right' => $naked_right,
                'glasses_left' => $glasses_left,
                'glasses_right' => $glasses_right,
                'mobile'=>$mobile,
                'grade'=>$grade,
                'classd'=>$classd,
                'report_date' => date('Y-m-d H:i:s', time()),
                'schoolname'=>$schooname['schoolname'],
                'updatetime'=>time(),
                'ethnicity'=>$ethnicity,
                'status'=>1
            ];
        
    
                 $insertResult =  DB::name('student_reports')->where('user_id', $auth->id)->where('task_id',$task_id)->update($reportData);
                   if ($insertResult) {
                    return $this->success('提交成功','/');
                } else {
                    // 获取最后一条SQL语句
               
                    // 返回具体的错误信息
                    return $this->error("提交异常。此账号不在上报列表");
                }
          
           

          
        };

        return $this->view->fetch();
    }
    /**
     * 计算年龄
     *
     * @param string $birthDate 出生日期字符串 (格式: YYYY-MM-DD)
     * @return int 年龄
     */
    public function calculateAge($birthDate) {
        // 创建一个 DateTime 对象表示出生日期
        $birthDateObject = new DateTime($birthDate);

        // 创建一个 DateTime 对象表示当前日期
        $currentDateObject = new DateTime('now');

        // 计算两个日期之间的差值
        $interval = $currentDateObject->diff($birthDateObject);

        // 返回年龄
        return $interval->y;
    }
}
