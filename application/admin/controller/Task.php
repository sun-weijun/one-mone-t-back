<?php

namespace app\admin\controller;

use app\common\controller\Backend;

use Exception;
use think\exception\PDOException;
use think\exception\ValidateException;
use think\Db;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class Task extends Backend
{

    /**
     * Task模型对象
     * @var \app\admin\model\Task
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Task;
    }


    /**
     * 添加
     *
     * @return string
     * @throws \think\Exception
     */
    public function add()
    {
        if (false === $this->request->isPost()) {

            return $this->view->fetch();
        }
        $params = $this->request->post('row/a');
        if (empty($params)) {
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $params = $this->preExcludeFields($params);


        if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
            $params[$this->dataLimitField] = $this->auth->id;
        }
        // 检查是否存在相同年份和月份的任务
        $existingTask = $this->model
            ->where('year', $params['year'])
            ->where('month', $params['month'])
            ->find();

        if ($existingTask) {
            $this->error(__('A task for the year %s and month %s already exists', $params['year'], $params['month']));
        }
        $result = false;
        Db::startTrans();
        try {
            //是否采用模型验证
            if ($this->modelValidate) {
                $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                $this->model->validateFailException()->validate($validate);
            }
            $result = $this->model->allowField(true)->save($params);
            if ($result) {
                // 查询学生表中的所有学生记录
                $students = Db::name('user')->select();
                  $schools = Db::name('school')->select();
                $schoolsById = [];
                foreach ($schools as $school) {
                    $schoolsById[$school['id']] = $school['schoolname'];
                }
                // 将学生记录插入到上报表中，并将“是否上报”字段设置为0
               $reports = [];
foreach ($students as $student) {
          if(isset($schoolsById[$student['school_id']])){
            $reports[] = [
                'user_id' => $student['id'],
                'name' => $student['nickname'],
                'year' => $params['year'],
                'grade' => $student['grade'],
                'classd' => $student['classd'],
                'schoolname' =>  $schoolsById[$student['school_id']]  ,
                'month' => $params['month'],
                'task_id' => $this->model->id,
                'idcard' => $student['username'],
                'createtime' => time(),
                // 'status' => 0
            ];
          }else{
            $reports[] = [
                'user_id' => $student['id'],
                'name' => $student['nickname'],
                'year' => $params['year'],
                'grade' => $student['grade'],
                'classd' => $student['classd'],
                'schoolname' =>  ''  ,
                'month' => $params['month'],
                'task_id' => $this->model->id,
                'idcard' => $student['username'],
                'createtime' => time(),
                // 'status' => 0
            ];
          }

   
}

if (!empty($reports)) {
    Db::name('student_reports')->insertAll($reports);
}
            }
            Db::commit();
        } catch (ValidateException | PDOException | Exception $e) {
            Db::rollback();
            
            $this->error($e->getMessage());
        }
        if ($result === false) {
            $this->error(__('No rows were inserted'));
        }
        $this->success();
    }


    /**
     * 删除的同时也删掉对应的一月一侧所有学生。
     *
     * @param $ids
     * @return void
     * @throws DbException
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     */
    public function del($ids = null)
    {
        if (false === $this->request->isPost()) {
            $this->error(__("Invalid parameters"));
        }
        $ids = $ids ?: $this->request->post("ids");
        if (empty($ids)) {
            $this->error(__('Parameter %s can not be empty', 'ids'));
        }
        $pk = $this->model->getPk();
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            $this->model->where($this->dataLimitField, 'in', $adminIds);
        }
        $list = $this->model->where($pk, 'in', $ids)->select();

        $count = 0;
        Db::startTrans();
        try {


            foreach ($list as $item) {
                $count += $item->delete();
            }
            $student_reports = Db::name('student_reports');
            foreach ($list as $item) {
               
              
                $student_reports->where('task_id', $item['id'])->delete();
            }
 


            Db::commit();
        } catch (PDOException|Exception $e) {
            Db::rollback();
            $this->error($e->getMessage());
        }
        if ($count) {
            $this->success();
        }
        $this->error(__('No rows were deleted'));
    }


    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */
}
