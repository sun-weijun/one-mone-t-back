<?php

return [
    'Id'         => 'ID',
    'Schoolname' => '学校名称',
    'Schoolsite' => '学校地址',
    'Xxlxr'      => '学校联系人',
    'Tel'        => '学校电话',
    'Createtime' => '创建时间',
    'Updatetime' => '更新时间'
];
