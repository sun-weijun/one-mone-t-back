<?php

return [
    'Id'            => 'id',
    'User_id'       => '用户id',
    'Name'          => '名字',
    'Report_date'   => '上报日期',
    'Year'          => '年',
    'Month'         => '月',
    'Naked_left'    => '裸左眼度数',
    'Naked_right'   => '裸右眼度数',
    'Glasses_left'  => '戴眼镜左眼度数',
    'Glasses_right' => '戴眼镜右眼度数',
    'Status'        => '状态',
    'Status 0'      => '未上报',
    'Set status to 0'=> '设为未上报',
    'Status 1'      => '已上报',
    'Set status to 1'=> '设为已上报',
    'Createtime'    => '创建时间',
    'Updatetime'    => '更新时间'
];
