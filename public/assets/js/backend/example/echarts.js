define(['jquery', 'bootstrap', 'backend', 'table', 'form', 'template', 'echarts', 'echarts-theme'], function ($, undefined, Backend, Table, Form, Template, Echarts) {

    var Controller = {
        index: function () {
          
            
            Form.api.bindevent($("#login-form1"), function (data, ret) {
               
                
                // 更新图表数据
                pieChart.setOption({
                        
                    series:[
                       { data:data.data,
                        type: 'pie',}
                    ]
                });
            }, );
            Form.api.bindevent($("#login-form2"), function (data, ret) {
               
             
                var option = {
                    xAxis: {
                        type: 'category',
                        data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
                    },
                    legend: {
                        data: ['戴镜右眼', '戴镜左眼', '裸眼左眼', '裸眼右眼']
                      },
                    yAxis: {
                        type: 'value'
                    },
                    series: data,
                    tooltip: {
                        trigger: 'item' // 完全禁用提示框
                    }
                };
    
                // 使用刚指定的配置项和数据显示图表。
                lineChart.setOption(option);
                
                // 更新图表数据
                // pieChart.setOption({
                        
                //     series:[
                //        { data:data.data,
                //         type: 'pie',}
                //     ]
                // });
            }, );
          
            //这句话在多选项卡统计表时必须存在，否则会导致影响的图表宽度不正确
            $(document).on("click", ".charts-custom a[data-toggle=\"tab\"]", function () {
                var that = this;
                setTimeout(function () {
                    var id = $(that).attr("href");
                    console.log(id);
                    
                    var chart = Echarts.getInstanceByDom($(id)[0]);
                    chart.resize();
                }, 0);
            });

            // 基于准备好的dom，初始化echarts实例
            var lineChart = Echarts.init(document.getElementById('line-chart'), 'walden');

            // 指定图表的配置项和数据
            var option = {
                xAxis: {
                    type: 'category',
                    data: ['一月', '二月', '三月', '四月', '五月', '六月', '七月','八月','九月','十月','十一月','十二月']
                },
                legend: {
                    data: ['戴镜右眼', '戴镜左眼', '裸眼左眼', '裸眼右眼']
                  },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    name:'戴镜右眼',
                    data: [0],
                    type: 'line'
                },
                {
                    name:'戴镜左眼',
                    data: [0],
                    type: 'line'
                },
                {
                    name:'裸眼左眼',
                    data: [0],
                    type: 'line'
                },
                {
                    name:'裸眼右眼',
                    data: [0],
                    type: 'line'
                },
            ],
                tooltip: {
                    trigger: 'item' // 完全禁用提示框
                }
            };

            // 使用刚指定的配置项和数据显示图表。
            lineChart.setOption(option);
            // 基于准备好的dom，初始化echarts实例
            var areaChart = Echarts.init(document.getElementById('area-chart'), 'walden');

            // 指定图表的配置项和数据
            var option = {
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日']
                },
                yAxis: {
                    type: 'value'
                },
                series: [{
                    data: [820, 932, 901, 934, 1290, 1330, 1320],
                    type: 'line',
                    areaStyle: {}
                }]
            };
          

            // 使用刚指定的配置项和数据显示图表。
            areaChart.setOption(option);

            var pieChart = Echarts.init(document.getElementById('pie-chart'), 'walden');
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b}: {c} ({d}%)'
                },
                legend: {
                    orient: 'vertical',
                    left: 10,
                    data: ['戴镜视力', '邮件营销', '联盟广告', '视频广告', '搜索引擎']
                },
                series: [
                    {
                        name: '',
                        type: 'pie',
                        radius: ['50%', '70%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '30',
                                    fontWeight: 'bold'
                                }
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: [
                            {value: 0, },
                            {value: 0, },
                            {value: 0, },
                            {value: 0, },
                            // {value: 234, name: '联盟广告'},
                            // {value: 135, name: '视频广告'},
                            // {value: 1548, name: '搜索引擎'}
                        ]
                    }
                ]
            };
            // 使用刚指定的配置项和数据显示图表。
            pieChart.setOption(option);
        


        }
    };
    return Controller;
});
