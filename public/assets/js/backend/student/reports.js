define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'student/reports/index' + location.search,
                    add_url: 'student/reports/add',
                    edit_url: 'student/reports/edit',
                    del_url: 'student/reports/del',
                    multi_url: 'student/reports/multi',
                    import_url: 'student/reports/import',
                    table: 'student_reports',
                    import_url: 'student/reports/import',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                fixedColumns: true,
                fixedRightNumber: 1,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'user_id', title: __('User_id')},
                        {field: 'idcard', title: '身份证号码'},
                        {field: 'name', title: __('Name'), operate: 'LIKE'},
                        {field: 'report_date', title: __('Report_date'), operate:'RANGE', addclass:'datetimerange', autocomplete:false},
                        {field: 'year', title: __('Year')},
                        {field: 'month', title: __('Month')},
                        {field: 'naked_left', title: __('Naked_left')},
                        {field: 'naked_right', title: __('Naked_right')},
                        {field: 'glasses_left', title: __('Glasses_left')},
                        {field: 'glasses_right', title: __('Glasses_right')},
                         {field: 'schoolname', title: '学校'},
                         {field: 'grade', title: '年级'},
                         {field: 'classd', title: '班级'},
                         {field: 'ethnicity', title: '民族'},
                        {field: 'status', title: __('Status'), searchList: {"0":__('Status 0'),"1":__('Status 1')}, formatter: Table.api.formatter.status},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'updatetime', title: __('Updatetime'), operate:'RANGE', addclass:'datetimerange', autocomplete:false, formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            $(document).on("change", "#c-user_id", function(){
              let _this =   $(this)
              $.ajax({
                url: "user/user/getById",
                data: {
                    id: _this.val(),
                    // 如果需要 CSRF 令牌，可以在这里添加
                    // _csrf_token: $('meta[name="csrf-token"]').attr('content')
                },
                method: "POST",
                success: function(data, ret) {
           
                    
                    $('#c-idcard').val(data.username);
                    $('#c-name').val(data.nickname)
                    $('#c-schoolname').val(data.school.schoolname)
                    $('#c-grade').val(data.grade)
                    $('#c-classd').val(data.classd)
            
                  
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.error("AJAX 请求失败: " + textStatus + ", 错误信息: " + errorThrown);
                    console.error(jqXHR.responseText);
                }
            });
                
            });
            Controller.api.bindevent();
        },
        edit: function () {
           
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
